# nxengine
Freeware / NXEngine build

sitelen Lasina only for NXEngine

[README.md (toki pona)](README.md)

README.jp.md (日本語)

[![Latest Release](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/badges/release.svg)](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/releases)

For Raspberry Pi and Android.

Not recommended for Desktops. Try [build for NXEngine-evo](https://gitlab.com/lupa-Tokusu-Monokatali/nxengine-evo)

## make install

```
git clone https://gitlab.com/lupa-Tokusu-Monokatali/nxengine.git
cd nxengine
```

Use one of the following:

- `make install LANG=en` sitelen Lasina only
- `make install LANG=jp` sitelen Ilakana only
- `make install LANG=` all

Retroarch:

Move build folder into `roms/nxengine/`
