# o toki ala e ni
MAKEFLAGS += --no-print-directory

DATESTAMP := $(shell date --iso-8601)

# Main project directory
# POKI_LAWA = .
ifneq ($(wildcard ../../.gitmodules),)
# $(info Found ../../.gitmodules)
# $(info $(shell grep "nasin/nxengine$$" "../../.gitmodules"))
ifeq ($(shell grep "nasin/nxengine$$" "../../.gitmodules"),)
POKI_LAWA = .
else
POKI_LAWA = ../..
endif

else
# $(info Could not find ../../.gitmodules)
POKI_LAWA = .
endif

# Translations
POKI_ANTE = $(POKI_LAWA)/ante

# Source game
MUSI_NANPA_WAN_PI_TOKI_NIJON = $(POKI_LAWA)/doukutsu
MUSI_NANPA_WAN_PI_TOKI_INLI  = $(POKI_LAWA)/CaveStory

# Build directory
POKI_PALI_SITELEN_LASINA  = $(POKI_LAWA)/pali/musi-Tokusu-pi-sitelen-Lasina
POKI_PALI_SITELEN_ILAKANA = $(POKI_LAWA)/pali/musi-Tokusu-pi-sitelen-Ilakana

# Dist directory
POKI_PINI            = $(POKI_LAWA)/pini
PINI_SITELEN_LASINA  = $(POKI_LAWA)/pini/musi-Tokusu-pi-sitelen-Lasina-$(DATESTAMP).zip
PINI_SITELEN_ILAKANA = $(POKI_LAWA)/pini/musi-Tokusu-pi-sitelen-Ilakana-$(DATESTAMP).zip

# Use `make LANG=` to make all translations
# Use `make LANG=en` for both sitelen-Lasina only
# Use `make LANG=ja` for sitelen-Ilakana (SJIS) only
# Default behavior is locale dependent
ifneq ($(filter en%, $(LANG)), )
	PALI_TARGETS = $(POKI_PALI_SITELEN_LASINA)
	PINI_TARGETS = $(PINI_SITELEN_LASINA)
else ifneq ($(filter jp%, $(LANG)), )
	PALI_TARGETS = $(POKI_PALI_SITELEN_ILAKANA)
	PINI_TARGETS = $(PINI_SITELEN_ILAKANA)
else
	PALI_TARGETS = $(POKI_PALI_SITELEN_LASINA) $(POKI_PALI_SITELEN_ILAKANA)
	PINI_TARGETS = $(PINI_SITELEN_LASINA) $(PINI_SITELEN_ILAKANA)
endif


.PHONY: all       ale ali  \
	clean     weka	   \
	build     pali     \
	dist      pini     \
	install   pana     \
	uninstall weka     \
	run       musi $(SITELEN_ILAKANA_TARGETS)



all: pali
ale: pali
ali: pali



build: pali
pali: $(PALI_TARGETS)

$(POKI_PALI_SITELEN_LASINA): $(POKI_ANTE)/toki-pona | $(POKI_PALI_SITELEN_LASINA)/ $(MUSI_NANPA_WAN_PI_TOKI_INLI)
	rsync -ruE --chmod=Fu=rw,Fg=r,Fo=r $(MUSI_NANPA_WAN_PI_TOKI_INLI)/* $(POKI_PALI_SITELEN_LASINA)
	cp -r $(POKI_ANTE)/toki-pona/* $(POKI_PALI_SITELEN_LASINA)
	dd if=$(POKI_ANTE)/toki-pona/csmap.bin conv=notrunc bs=1 seek=604080 of=$(POKI_PALI_SITELEN_LASINA)/Doukutsu.exe
$(POKI_PALI_SITELEN_LASINA)/:
	mkdir -p $@

$(POKI_PALI_SITELEN_ILAKANA): $(POKI_ANTE)/sitelen-Ilakana-SJIS | $(POKI_PALI_SITELEN_ILAKANA)/ $(MUSI_NANPA_WAN_PI_TOKI_NIJON) $(POKI_PALI_SITELEN_ILAKANA)/locale-ja/ja_JP.SJIS
	rsync -ruE --chmod=Fu=rw,Fg=r,Fo=r $(MUSI_NANPA_WAN_PI_TOKI_NIJON)/* $(POKI_PALI_SITELEN_ILAKANA)
	cp -r $(POKI_ANTE)/sitelen-Ilakana-SJIS/* $(POKI_PALI_SITELEN_ILAKANA)
	dd if=$(POKI_ANTE)/sitelen-Ilakana-SJIS/csmap.bin conv=notrunc bs=1 seek=604080 of=$(POKI_PALI_SITELEN_ILAKANA)/Doukutsu.exe
$(POKI_PALI_SITELEN_ILAKANA)/:
	mkdir -p $@
$(POKI_PALI_SITELEN_ILAKANA)/locale-ja/ja_JP.SJIS: | $(POKI_PALI_SITELEN_ILAKANA)/locale-ja/
	-@localedef -c -f SHIFT_JIS -i ja_JP $@ 2> /dev/null
$(POKI_PALI_SITELEN_ILAKANA)/locale-ja/:
	mkdir -p $@



ante/toki-pona:
	git submodule update --init --rebase --remote
ante/sitelen-Ilakana-SJIS:
	git submodule update --init --rebase --remote



run: musi
ifeq (,$(filter jp%, $(LANG)))
musi:
	@cd $(POKI_PALI_SITELEN_LASINA); wine Doukutsu.exe
else
musi:
	@cd $(POKI_PALI_SITELEN_ILAKANA); env LOCPATH=locale-ja LANG=ja_JP.SJIS wine Doukutsu.exe
endif



dist: pini
pini: $(PINI_TARGETS)

$(POKI_PINI): $(PINI_TARGETS)
$(POKI_PINI)/:
	@mkdir -p $@

$(PINI_SITELEN_LASINA): $(POKI_ANTE)/toki-pona $(POKI_PINI)/musi-Tokusu-pi-sitelen-Lasina/
ifdef ($@)
	@rm -f $@
endif
	cd $(POKI_PINI); zip -rm $(notdir $@) musi-Tokusu-pi-sitelen-Lasina 1> /dev/null
$(POKI_PINI)/musi-Tokusu-pi-sitelen-Lasina/:
	mkdir -p $@
	rsync -ruE --chmod=Fu=rw,Fg=r,Fo=r $(MUSI_NANPA_WAN_PI_TOKI_INLI)/* $@
	cp README.md $@
	cp README.en.md $@
	cp -r $(POKI_ANTE)/toki-pona/* $@
	dd if=$(POKI_ANTE)/toki-pona/csmap.bin conv=notrunc bs=1 seek=604080 of=$@/Doukutsu.exe

$(PINI_SITELEN_ILAKANA): $(POKI_ANTE)/sitelen-Ilakana-SJIS $(POKI_PINI)/musi-Tokusu-pi-sitelen-Ilakana-SJIS/
ifdef ($@)
	@rm -f $@
endif
	cd $(POKI_PINI); zip -rm $(notdir $@) musi-Tokusu-pi-sitelen-Ilakana-SJIS 1> /dev/null
$(POKI_PINI)/musi-Tokusu-pi-sitelen-Ilakana-SJIS/: | $(POKI_PINI)/musi-Tokusu-pi-sitelen-Ilakana-SJIS/locale-ja/ja_JP.SJIS
	mkdir -p $@
	rsync -ruE --chmod=Fu=rw,Fg=r,Fo=r $(MUSI_NANPA_WAN_PI_TOKI_NIJON)/* $@
	cp README.md $@
	cp README.en.md $@
	cp -r $(POKI_ANTE)/sitelen-Ilakana-SJIS/* $@
	dd if=$(POKI_ANTE)/sitelen-Ilakana-SJIS/csmap.bin conv=notrunc bs=1 seek=604080 of=$@/Doukutsu.exe
$(POKI_PINI)/musi-Tokusu-pi-sitelen-Ilakana-SJIS/locale-ja/ja_JP.SJIS: | $(POKI_PINI)/musi-Tokusu-pi-sitelen-Ilakana-SJIS/locale-ja/
	-@localedef -c -f SHIFT_JIS -i ja_JP $@ 2> /dev/null
$(POKI_PINI)/musi-Tokusu-pi-sitelen-Ilakana-SJIS/locale-ja/:
	mkdir -p $@


clean: weka
weka:
	rm -rf $(POKI_PALI_SITELEN_LASINA)
	rm -rf $(POKI_PALI_SITELEN_ILAKANA)
	rm -rf $(PINI_SITELEN_LASINA)
	rm -rf $(PINI_SITELEN_ILAKANA)



$(POKI_LAWA)/doukutsu: | $(POKI_LAWA)/dou_1006.zip
	@cd $(POKI_LAWA); unzip 'dou_1006.zip' 1> /dev/null

$(POKI_LAWA)/dou_1006.zip: | $(POKI_LAWA)
	@cd $(POKI_LAWA); wget 'https://studiopixel.jp/binaries/dou_1006.zip' 1> /dev/null

$(POKI_LAWA)/CaveStory: | $(POKI_LAWA)/cavestoryen.zip
	@cd $(POKI_LAWA); unzip 'cavestoryen.zip' 1> /dev/null

$(POKI_LAWA)/cavestoryen.zip: | $(POKI_LAWA)
	@cd $(POKI_LAWA); wget 'https://www.cavestory.org/downloads/cavestoryen.zip' 1> /dev/null
