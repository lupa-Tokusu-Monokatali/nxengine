# nxengine
nasin Freeware anu nasin NXEngine

nasin NXEngine li jo e sitelen Lasina taso

[README.en.md (English)](README.en.md)

README.jp.md (日本語)

[![Latest Release](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/badges/release.svg)](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/releases)

pona tawa ilo Raspberry Pi anu ilo Andriod.

ilo supa la, ni li ike. o alasa kepeken e [nasin NXEngine-evo](https://gitlab.com/lupa-Tokusu-Monokatali/nxengine-evo)

## make pana

```
git clone https://gitlab.com/lupa-Tokusu-Monokatali/nxengine.git
cd nxengine
```

o kepeken wan e ni:

- `make pana LANG=en` sitelen Lasina taso
- `make pana LANG=jp` sitelen Ilakana taso
- `make pana LANG=` ale

Retroarch:

o tawa e poki pali lon poki `roms/nxengine/`
